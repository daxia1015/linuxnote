#!/usr/bin/sh

cd /home/yarn

declare -i line=$RANDOM*3403/32767
tab=$(sed -n ${line},${line}p .vars | awk -F ',' '{print $1}')
col=$(sed -n ${line},${line}p .vars | awk -F ',' '{print $2}')

declare -i uid=$RANDOM*$(date +%s)/100/32767
md5=$(echo -n $uid|md5sum|cut -d ' ' -f1)
key=${md5:0:4}_$uid

echo $tab $col $uid $key

/home/yarn/.alihbase/bin/hbase shell << eof
delete '$tab','$key','cf:$col'

exit
eof

